# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By:  <@student.42.fr>                          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/08/30 21:01:20 by                   #+#    #+#              #
#    Updated: 2016/09/02 23:36:06 by tlovato          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fdf
INC_DIR = ./includes/
SRCS_DIR = ./srcs/

SRCS_FILES = fdf.c get_values.c draw_line.c

SRCS = $(addprefix $(SRCS_DIR), $(SRCS_FILES))

OBJS = $(SRCS:.c=.o)

all: $(NAME)

$(NAME) : $(OBJS)
			@echo "I'M MAKING OBJEEEEEEEECTS !!!!!! : \033[1;32m DONE !\033[m"
			@make -C libft/
			@gcc -o $@ $^ -L libft/ -lft -L minilibx_macos/ -lmlx -framework OpenGL -framework AppKit -fsanitize=address
			@echo "I MAAAAAKIIIIIIING $(NAME) !!!!!!!!! : \033[1;32m DONE !\033[m"

clean:
			@rm -f $(OBJS)
			@echo "I'M CLEANIIIIIIIIING !!!!!!! : \033[1;31m DONE !\033[m"

fclean: clean
			@rm -f $(NAME)
			@cd libft && make fclean
			@echo "I'M FCLEANIIIIIIIIING !!!!!! : \033[1;31m DONE !\033[m"

re: fclean all

.PHONY: all clean fclean re
