/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/14 13:16:32 by tlovato           #+#    #+#             */
/*   Updated: 2015/12/15 20:02:30 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*nlst;
	t_list	*link;

	nlst = NULL;
	link = NULL;
	while (lst != NULL)
	{
		if (nlst == NULL)
		{
			nlst = f(lst);
			link = nlst;
		}
		else
			link->next = f(lst);
		lst = lst->next;
	}
	return (nlst);
}
