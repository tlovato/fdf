/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_line.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/19 12:21:01 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/19 12:29:45 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fdf.h"

void			draw_line(t_ptr *ptr, t_values v, int nextx, int nexty)
{
	double		dx;
	double		dy;
	double		dd;
	int			i;

	dx = (v.absc < nextx) ? nextx - v.absc : v.absc - nextx;
	dy = (v.ord < nexty) ? nexty - v.ord : v.ord - nexty;
	dd = (dx > dy) ? dx : dy;
	i = 0;
	dx = nextx - v.absc;
	dy = nexty - v.ord;
	while (i <= dd)
	{
		mlx_pixel_put(ptr->mlx, ptr->win, v.absc, v.ord, 0x00FF00FF);
		v.absc += dx / dd;
		v.ord += dy / dd;
		i++;
	}
}

void			draw_absc(t_values v, int o, int z, t_ptr *ptr)
{
	v.nexto = ((o - z) * v.zoomy) + v.center;
	draw_line(ptr, v, v.absc + v.zoomx, v.nexto);
}

void			draw_ord(t_values v, int o, int z, t_ptr *ptr)
{
	v.nexto = (((o + 1) - z) * v.zoomy) + v.center;
	draw_line(ptr, v, v.absc, v.nexto);
}
