/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libftprintf.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/23 11:54:02 by tlovato           #+#    #+#             */
/*   Updated: 2016/05/23 11:54:03 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFTPRINTF_H
# define LIBFTPRINTF_H

# include <stdarg.h>
# include <stdlib.h>
# include <string.h>
# include <stdio.h>
# include <unistd.h>
# include <wchar.h>
# include <stdint.h>
# include "libft.h"

# define YO printf("\nYO\n");
# define ARG printf("arg = [%s]\n", ret->arg);
# define CONV printf("conv = [%s]\n", ret->conv);
# define SUB printf("sub = %s\n", ret->sub);
# define TMP printf("tmp = %s\n", tmp);
# define RES printf("res = %s\n", ret->res);

typedef struct			s_flag
{
	int					sharp;
	int					zero;
	int					dash;
	int					plus;
	int					space;
	int					is_width;
	int					width;
	int					is_prec;
	int					prec;
	int					i;
}						t_flag;

typedef struct			s_set
{
	int					len;
	int					type;
	char				*modif;
	int					percent;
	int					index;
}						t_set;

typedef struct			s_result
{
	char				*cpy;
	char				*arg;
	char				*res;
	int					res_len;
	char				*sub;
	int					sub_len;
	int					perc;
	int					len;
	char				*conv;
	int					conv_len;
	int					argument;
	t_flag				argum;
	int					i;
	int					percent;
	t_set				set;
}						t_result;

typedef struct			s_type
{
	char				*wide;
	size_t				ret;
	wint_t				cmaj;
	void				*p;
	char				*s;
	wchar_t				*smaj;
	int					percent;
	intmax_t			si_conv;
	uintmax_t			usi_conv;
}						t_type;

int						ft_printf(const char *restrict format, ...);
void					attrib_case(t_result *ret, va_list ap, char *cpy);
void					attrib_values_ret(t_result *ret);
void					attrib_values_set(t_set *values);
void					attrib_val_flags(t_flag *argum);
int						get_digits(char *arg, int i);
int						calcul_perc(t_result *ret);
int						verif_convtype(char *arg);
int						is_convtype(int i);
int						is_flag(int i);
void					attrib_values_type(t_type *pars);
int						is_modif(int i);
char					*lower_str(char *str);
void					take_invalid_arg(t_result *ret, int i);
unsigned int			count_len(unsigned int n);
char					*bin_to_str(unsigned int n);
char					*wstr_convert(wchar_t *wstr);
int						is_arg(int i);
int						is_precision(int i);
int						is_width(int i);
int						verif_invalid_arg(char *arg);
char					*ft_llitoa(long long nb);
char					*ft_llitoa_base(long long nb, int base);
char					*ft_ullitoa(unsigned long long nb);
char					*ft_ullitoa_base(unsigned long long nb, int b);
void					free_result(t_result *str);
void					set_types_values(t_set *values, char *arg);
void					convtype_case(t_result *ret, va_list ap);
void					pars_arg(va_list ap, t_result *ret);
void					pars_print_conv(t_type *pars, t_result *ret);
void					attrib_convtype(t_result *ret, va_list ap);
void					invalid_arg(t_result *ret, int i);
void					hh_conv(t_type *pars, va_list ap, t_set values);
void					h_conv(t_type *pars, va_list ap, t_set values);
void					ll_conv(t_type *pars, va_list ap, t_set values);
void					l_conv(t_type *p, va_list ap, t_set v, t_result *r);
void					zj_conv(t_type *pars, va_list ap, t_set values);
void					print_cn_zero(t_result *ret);
void					print_cn_width(t_result *ret);
void					inv_arg_case(t_result *ret);
char					*w_convert(unsigned int c);
void					attrib_flags(char *arg, t_flag *argum);
void					print_flags(t_result *ret, t_flag argum, t_set values);
void					prec_udigits(t_flag argum, t_result *ret, t_set values);
void					width_udigits(t_flag argum, t_result *ret);
void					flags_str(t_result *ret, t_flag argum, t_set values);
void					flags_p(t_result *ret, t_flag argum);
void					no_percent(t_result *ret);

#endif
