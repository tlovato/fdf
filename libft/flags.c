/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   flags.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/31 19:33:19 by tlovato           #+#    #+#             */
/*   Updated: 2016/05/31 19:33:22 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

static void		prec_idigits(int prec, t_result *ret)
{
	int		i;
	int		len;
	char	*tmp;

	if (ret->conv[0] == '0' && prec == 0)
	{
		ret->conv[0] = '\0';
		return ;
	}
	i = ret->conv[0] == '-';
	len = prec - (ft_strlen(ret->conv) - i);
	if (len <= 0)
		return ;
	tmp = ft_strnew(prec + i);
	if (i)
		tmp[0] = ret->conv[0];
	ft_memset(&tmp[i], '0', len);
	ft_strcat(tmp, &ret->conv[!ft_isdigit(tmp[0])]);
	free(ret->conv);
	ret->conv = tmp;
}

static void		width_idigits(t_flag argum, t_result *ret)
{
	int			i;
	int			j;
	int			len;
	char		*tmp;
	int			k;

	i = !ft_isdigit(ret->conv[0]);
	k = argum.dash == 0 && argum.zero == 1 && argum.is_prec == 0;
	i = i && k;
	len = argum.width - ft_strlen(ret->conv);
	if (len <= 0)
		return ;
	j = argum.dash == 1 ? ft_strlen(ret->conv) : 0;
	tmp = ft_strnew(argum.width);
	if (i)
		tmp[0] = ret->conv[0];
	ft_memset(&tmp[i ? i : j], argum.zero == 1 ? '0' : ' ', len);
	if (j && !i)
		ft_memcpy(tmp, ret->conv, ft_strlen(ret->conv));
	else
		ft_strcat(tmp, &ret->conv[i]);
	free(ret->conv);
	ret->conv = tmp;
}

static void		flags_unsigned(t_result *ret, t_flag a, t_set v)
{
	char		*tmp;

	if (a.sharp == 1 && (v.type == 'x' || v.type == 'X') && ret->conv[0] != '0')
	{
		tmp = ft_strjoin(v.type == 'x' ? "0x" : "0X", ret->conv);
		free(ret->conv);
		ret->conv = tmp;
	}
	if (a.is_prec == 1)
		prec_udigits(a, ret, v);
	if (a.sharp == 1 && (v.type == 'o') && (ret->conv[0] != '0'))
	{
		tmp = ft_strjoin("0", ret->conv);
		free(ret->conv);
		ret->conv = tmp;
	}
	if (a.is_width == 1)
		width_udigits(a, ret);
}

static void		flags_di(t_result *ret, t_flag argum, t_set values)
{
	if (argum.is_prec == 1)
		prec_idigits(argum.prec, ret);
	if ((argum.plus == 1 || argum.space == 1) && ret->conv[0] != '-')
		ret->conv = ft_strjoin(argum.plus == 1 ? "+" : " ", ret->conv);
	if (argum.is_width == 1)
		width_idigits(argum, ret);
}

void			print_flags(t_result *ret, t_flag argum, t_set values)
{
	if (argum.is_prec == 1 || argum.dash == 1)
		argum.zero = 0;
	if (values.type == 'd' || values.type == 'i')
		flags_di(ret, argum, values);
	else if (values.type == 'x' || values.type == 'X'
		|| values.type == 'o' || values.type == 'u')
		flags_unsigned(ret, argum, values);
	else if (values.type == 's' || values.type == 'c')
		flags_str(ret, argum, values);
	else if (values.type == 'p')
		flags_p(ret, argum);
}
