/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_conv.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/25 14:52:04 by tlovato           #+#    #+#             */
/*   Updated: 2016/05/25 14:52:05 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

static void		print_c_conv(int c, t_result *ret)
{
	if (ret->conv)
		free(ret->conv);
	ret->conv = ft_strnew(1);
	ret->conv[0] = c;
	ret->conv_len = 1;
}

static void		print_str_conv(char *str, t_result *ret)
{
	if (ret->conv)
		free(ret->conv);
	if (str == NULL)
		ret->conv = ft_strdup("(null)");
	else
		ret->conv = ft_strdup(str);
	ret->conv_len = (ret->set.type == 'c' && str[0] == '\0');
	ret->conv_len += ft_strlen(ret->conv);
}

static void		print_p_conv(void *ptr, t_result *ret)
{
	char		*tmp;

	if (ret->conv)
		free(ret->conv);
	ret->conv = ft_ullitoa_base((unsigned long long)ptr, 16);
	lower_str(ret->conv);
	tmp = ft_strjoin("0x", ret->conv);
	free(ret->conv);
	ret->conv = tmp;
	ret->conv_len = ft_strlen(ret->conv);
}

static void		print_unsigned_conv(unsigned long long n, t_result *ret)
{
	if (ret->conv)
		free(ret->conv);
	if (ret->set.type == 'x' || ret->set.type == 'X')
	{
		ret->conv = ft_ullitoa_base(n, 16);
		if (ret->set.type == 'x')
			lower_str(ret->conv);
	}
	if (ret->set.type == 'o' || ret->set.type == 'O')
		ret->conv = ft_ullitoa_base(n, 8);
	if (ret->set.type == 'u' || ret->set.type == 'U')
		ret->conv = ft_ullitoa_base(n, 10);
	if (ret->set.type == 'b')
		ret->conv = ft_llitoa_base(n, 2);
	ret->conv_len = ft_strlen(ret->conv);
}

void			pars_print_conv(t_type *pars, t_result *r)
{
	int			i;

	i = r->set.type == 's' && r->set.modif == NULL;
	if (r->set.type == 'c' && !ft_strequ(r->set.modif, "l"))
		print_c_conv(pars->si_conv, r);
	else if (r->set.type == 's' || (r->set.type == 'c'
		&& ft_strequ(r->set.modif, "l")))
		print_str_conv(i ? pars->s : pars->wide, r);
	else if (r->set.type == 'd' || r->set.type == 'i')
		r->conv = ft_llitoa(pars->si_conv);
	else if (r->set.type == 'u' || r->set.type == 'o'
		|| r->set.type == 'x' || r->set.type == 'X' || r->set.type == 'b')
		print_unsigned_conv(pars->usi_conv, r);
	else if (r->set.type == 'p')
		print_p_conv(pars->p, r);
}
