/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   modifs.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/05/25 11:14:09 by tlovato           #+#    #+#             */
/*   Updated: 2016/05/25 11:14:10 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftprintf.h"

void		hh_conv(t_type *pars, va_list ap, t_set values)
{
	if (values.type == 'd' || values.type == 'i')
		pars->si_conv = (char)va_arg(ap, int);
	else if (values.type == 'x' || values.type == 'X' ||
		values.type == 'u' || values.type == 'o')
		pars->usi_conv = (unsigned char)va_arg(ap, unsigned);
	else if (values.type == 'p')
		pars->p = va_arg(ap, void *);
}

void		h_conv(t_type *pars, va_list ap, t_set values)
{
	if (values.type == 'd' || values.type == 'i')
		pars->si_conv = (short)va_arg(ap, int);
	if (values.type == 'o' || values.type == 'x' || values.type == 'X'
		|| values.type == 'u')
		pars->usi_conv = (unsigned short)va_arg(ap, unsigned);
	else if (values.type == 'p')
		pars->p = va_arg(ap, void *);
}

void		ll_conv(t_type *pars, va_list ap, t_set values)
{
	if (values.type == 'd' || values.type == 'i')
		pars->si_conv = va_arg(ap, long long);
	if (values.type == 'o' || values.type == 'x'
		|| values.type == 'X' || values.type == 'u')
		pars->usi_conv = va_arg(ap, unsigned long long);
	else if (values.type == 'p')
		pars->p = va_arg(ap, void *);
}

void		l_conv(t_type *pars, va_list ap, t_set values, t_result *ret)
{
	if (values.type == 'd' || values.type == 'i')
		pars->si_conv = va_arg(ap, long);
	if (values.type == 'c')
	{
		pars->cmaj = va_arg(ap, wint_t);
		ret->conv_len = pars->cmaj == 0 ? 1 : 0;
		pars->wide = w_convert(pars->cmaj);
	}
	else if (values.type == 's')
	{
		pars->smaj = va_arg(ap, wchar_t *);
		pars->wide = wstr_convert(pars->smaj);
	}
	else if (values.type == 'p')
		pars->p = va_arg(ap, void *);
	if (values.type == 'x' || values.type == 'X'
		|| values.type == 'u' || values.type == 'o')
		pars->usi_conv = va_arg(ap, unsigned long);
}

void		zj_conv(t_type *pars, va_list ap, t_set values)
{
	if (values.type == 'd' || values.type == 'i')
		pars->si_conv = va_arg(ap, intmax_t);
	if (values.type == 'X' || values.type == 'x' || values.type == 'u'
		|| values.type == 'o')
		pars->usi_conv = va_arg(ap, uintmax_t);
}
