/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_values.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/31 15:00:33 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/19 12:04:06 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/fdf.h"

static int		calcul_y(char *path)
{
	char		*line;
	int			y;
	int			fd;

	y = 0;
	fd = open(path, O_RDONLY);
	while (get_next_line(fd, &line) && line)
		y++;
	return (y);
}

static int		calcul_x(char *path)
{
	char		**tab;
	int			x;
	char		*line;
	int			fd;

	x = 0;
	fd = open(path, O_RDONLY);
	get_next_line(fd, &line);
	if (line)
	{
		tab = ft_strsplit(line, ' ');
		while (tab[x])
			x++;
	}
	return (x);
}

int				get_values(char *path, struct s_values *values)
{
	values->x = calcul_x(path);
	values->y = calcul_y(path);
	values->zoomx = 30;
	values->zoomy = 20;
	values->center = 300;
	if (values->x == 0 && values->y == 0)
		return (0);
	return (1);
}

int				**get_map(char *path, int y, int x, int fd)
{
	char		*line;
	int			i;
	int			j;
	int			**map;
	char		**tmp;

	j = 0;
	map = (int **)malloc(sizeof(int *) * y);
	fd = open(path, O_RDONLY);
	while (j < y && get_next_line(fd, &line))
	{
		tmp = ft_strsplit(line, ' ');
		i = 0;
		map[j] = (int *)malloc(sizeof(int) * x);
		while (i < x)
		{
			map[j][i] = ft_atoi(tmp[i]);
			i++;
		}
		j++;
	}
	close(fd);
	return (map);
}

void			calc_values(t_values *val, int z, int a, int o)
{
	val->absc = (a * val->zoomx) + val->center;
	val->ord = ((o - z) * val->zoomy) + val->center;
}
