/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <tlovato@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/30 22:08:14 by tlovato           #+#    #+#             */
/*   Updated: 2016/09/19 12:30:10 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include <fcntl.h>
# include <errno.h>
# include <string.h>
# include <stdio.h>
# include "libft.h"
# include "libftprintf.h"
# include "../minilibx_macos/mlx.h"

# define P(nb) ft_printf("%d\n", nb);
# define PS(str) ft_printf("%s\n", str);

typedef struct		s_ptr
{
	void			*mlx;
	void			*win;
	void			*image;
}					t_ptr;

typedef struct		s_values
{
	int				x;
	int				y;
	int				z;
	double			absc;
	double			ord;
	int				nexta;
	int				nexto;
	int				zoomx;
	int				zoomy;
	int				center;
}					t_values;

typedef struct		s_all
{
	t_ptr			ptr;
	t_values		value;
	int				**map;
}					t_all;

int					get_values(char *path, struct s_values *values);
int					**get_map(char *path, int y, int x, int fd);
void				calc_values(t_values *val, int z, int a, int o);
void				display(t_values v, int **map, t_ptr *ptr);
void				draw_line(t_ptr *ptr, t_values v, int nextx, int nexty);
void				draw_absc(t_values v, int o, int z, t_ptr *ptr);
void				draw_ord(t_values v, int o, int z, t_ptr *ptr);

#endif
