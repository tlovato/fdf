/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tlovato <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/15 20:14:48 by tlovato           #+#    #+#             */
/*   Updated: 2016/06/15 20:14:55 by tlovato          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_strrev(char *str)
{
	char		*rev;
	int			i;
	int			len;

	len = ft_strlen(str) - 1;
	rev = (char *)malloc(sizeof(char) * len + 1);
	i = 0;
	while (i < (int)ft_strlen(str))
	{
		rev[i] = str[len];
		i++;
		len--;
	}
	rev[i] = '\0';
	return (rev);
}
